import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public storage: Storage
  ) {

  }

  logOff() {
    this.storage.remove('usuario');
    this.navCtrl.push('LoginPage');    
  }

}
